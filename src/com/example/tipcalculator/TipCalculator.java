package com.example.tipcalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class TipCalculator extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tip_calculator);
		initDisplayButton();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tip_calculator, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initDisplayButton(){
		ImageButton displayButton = (ImageButton) findViewById( R.id.imageButton1); 
		displayButton.setOnClickListener( new OnClickListener () { 
			
			@Override 
			public void onClick( View arg0) {
			
			EditText billamount = (EditText)findViewById( R.id.editText1);
			EditText tippercentage = (EditText)findViewById( R.id.editText2);
			
			String strbill = billamount.getText().toString();
			String strtippc = tippercentage.getText().toString();
			float bill = Float.parseFloat(strbill);
			float tippc = Float.parseFloat(strtippc);
			
			float tip = (bill*tippc)/100;
			String tipToPay = String.valueOf(tip);			
			EditText tipamount = (EditText)findViewById( R.id.editText4);
			tipamount.setText(tipToPay);
			
			
		}

	});
	}

}
